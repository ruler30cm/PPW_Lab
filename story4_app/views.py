from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from story4_app.models import Jadwal, UserModel
from . import forms
import json
from django.core import serializers
# Create your views here.

def index(request):
    return render(request, 'story4_app/index.html')

def experiences(request):
    return render(request, 'story4_app/experiences.html')

def education(request):
    return render(request, 'story4_app/education.html')

def contact(request):
    return render(request, 'story4_app/contactme.html')

def skills(request):
    return render(request, 'story4_app/skills.html')

def register(request):
    if request.method == "POST":
        form = forms.UserForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('register')
    else:
        form = forms.UserForm()
            
    return render(request, 'story4_app/register.html', {'form' : form})

def validate_register(request):
    email = request.GET.get('email')
    data = {
        'is_taken' : UserModel.objects.filter(email__iexact=email).exists()
    }
    print(email)
    if data['is_taken']:
        data['error'] = "An User with this email already exists!"
    return JsonResponse(data)

def add_schedule(request):
    if request.method == "POST":
        form = forms.FormJadwal(request.POST)
        if form.is_valid():
            form.save()
            return redirect('schedule')
    else:
        form = forms.FormJadwal()

    return render(request, 'story4_app/addschedule.html', {'form':form})

def subscriber(request):
    nameval = request.GET.get('name')
    emailval = request.GET.get('email')
    passw = request.GET.get('password')
    deleted = False
    if (UserModel.objects.filter(name__iexact=nameval, email__iexact=emailval, password__iexact=passw).exists()):
        UserModel.objects.get(name=nameval, email=emailval).delete()
        deleted = True
        dictionaries = [ obj.as_dict() for obj in UserModel.objects.all() ] + [deleted]
        return JsonResponse(dictionaries, safe=False)
    dictionaries = [ obj.as_dict() for obj in UserModel.objects.all() ] + [deleted]
    return JsonResponse(dictionaries, safe=False)

def schedule(request):
    jadwal_list = Jadwal.objects.order_by('tanggal', "jam")
    jadwal_dict = {'jadwal':jadwal_list}
    return render(request, 'story4_app/schedule.html', context=jadwal_dict)

def delete_schedule(request):
    jadwal_list = Jadwal.objects.all().delete()
    return render(request, 'story4_app/schedule.html', {'jadwal_list' : jadwal_list})