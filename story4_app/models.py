from django.db import models

# Create your models here.
class Jadwal(models.Model):
    nama_kegiatan = models.CharField(max_length=264)
    tempat = models.CharField(max_length=264)
    kategori = models.CharField(max_length=264)
    tanggal = models.DateField()
    jam = models.TimeField()

class UserModel(models.Model):
    name = models.CharField(max_length=300)
    email = models.EmailField(max_length=254)
    password = models.CharField(max_length=50)
    
    def as_dict(self):
        return {
            "name" : self.name,
            "email" : self.email,
            "pk" : self.pk,
        }