var listSubs = [];

function register_form_validation(bool) {
    $(".register-form").validate();
    return $(".register-form").valid();
}

function toggle_disable_button(bool) {
    if (bool) {
        $("#Submit").prop('disabled', false);
    } else {
        $("#Submit").prop('disabled', true);
    }
}

function deleteSubscriber(id) {
    var name = document.getElementById(id.id + "name").innerHTML;
    var email = document.getElementById(id.id + "email").innerHTML;
    var pass = window.prompt("Are you sure, " +name+ "? Please enter the password:");
    $.ajax({
        type: "GET",
        url: subscriber,
        data : {
            "name" : name,
            "email" : email,
            "password" : pass,
        },
        success: function(data) {
            console.log(data[data.length-1])
            if (data[data.length-1] == false) {
                alert("Your password is wrong");
            } else {
                alert("Deleted");
            }
        }
    })
    setTimeout(function(){
        getSubscriber()
    }, 1000)
}

function getSubscriber() {
    $("#subscribers").empty();
    $.ajax({
        type: "GET",
        url: subscriber,
        success: function(data) {
            var tr = [];
            var countersubs = 0;
            tr.push("<thead><th>Nama Subscriber</th><th>Email Subscriber</th><th>Delete</th></thead>");
            for (countersubs; countersubs < data.length-1; countersubs++) {
                tr.push("<tr id='" + countersubs + "'>");
                tr.push("<td id='" + countersubs + "name'>" + data[countersubs].name + "</td>");
                tr.push("<td id='" + countersubs + "email'>" + data[countersubs].email + "</td>");
                tr.push("<td id='" + countersubs + "button'>" + '<input onclick="deleteSubscriber(this)" type="button" value="Delete" class="btn btn-primary" id="' + (countersubs) + '">' + "</td>")
                tr.push("</tr>");
            }
            $("#subscribers").append($(tr.join('')));
        }
    })
}

$(function(){

    var csrftoken = Cookies.get('csrftoken');
    toggle_disable_button(false);

    getSubscriber();

    $(".register-form").keyup(function() {
        toggle_disable_button(false);
        $("#id_name").keyup(function() {
            toggle_disable_button(register_form_validation());
        })

        $("#id_password").keyup(function() {
            toggle_disable_button(register_form_validation());
        })

        var email = $("#id_email").val();
        $.ajax({
            url: urlvalid,
            data: {
                'email' : email,
            },
            dataType : 'json',
            success: function(data) {
                toggle_disable_button(register_form_validation() && !data.is_taken);
                if (data.is_taken && !document.getElementById("emailtaken")) {
                    $("#id_email").after("<p id='emailtaken' >Email is taken by another user!</p>");
                } else {
                    if (!data.is_taken) {
                        $("#emailtaken").remove();
                    }
                }
            }
        });
    });

    $('.register-form').on('submit', function(event){
        event.preventDefault();
        create_account();
        setTimeout(function() {
            getSubscriber();
        }, 1000);
    });

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    function create_account() {
        console.log("create account is working!") // sanity check
        var name = $("#id_name").val();
        var email = $('#id_email').val();
        var password = $('#id_password').val();

        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            }
        });

        $.ajax({
            url : register, // the endpoint
            type : "POST", // http method
            data : { 'name' : name, 'email' : email, 'password' : password }, // data sent with the post request
            // handle a successful response
            success : function(data) {
                alert("You are successfully registered!, " + name);
            },
            error : function(data) {
                alert("There's an error going on!");
            }
        });
    };


});