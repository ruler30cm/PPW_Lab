# Generated by Django 2.1.1 on 2018-10-03 04:47

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Jadwal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama_kegiatan', models.CharField(max_length=264, unique=True)),
                ('tempat', models.CharField(max_length=264, unique=True)),
                ('kategori', models.CharField(max_length=264, unique=True)),
                ('tanggal', models.DateField()),
                ('jam', models.TimeField()),
            ],
        ),
    ]
