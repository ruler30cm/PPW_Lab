from django import forms
from .models import Jadwal, UserModel
from django.forms import ModelForm

class FormJadwal(forms.ModelForm):
    nama_kegiatan = forms.CharField(max_length=264)
    tempat = forms.CharField(max_length=264)
    kategori = forms.CharField(max_length=264)
    tanggal = forms.DateField(widget=forms.DateInput(attrs={"type" : 'date'}))
    jam = forms.TimeField(widget=forms.TimeInput(attrs={'type' : 'time'}))

    class Meta:
        model = Jadwal
        fields = ("nama_kegiatan", "tempat", "kategori", "tanggal", "jam",)

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    class Meta:
        model = UserModel
        fields = ("name", "email", "password")