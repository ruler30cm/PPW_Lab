from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.utils.timezone import now
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import json
import requests
from . import views, models, forms

# Create your tests here.
class Story10Test(TestCase):

    def test_if_name_is_exist(self):
        test = "Muhammad Rizki Darmawan"
        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_if_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_if_url_experiences_is_exist(self):
        response = Client().get('/experiences/')
        self.assertEqual(response.status_code, 200)

    def test_if_url_contact_is_exist(self):
        response = Client().get('/contactme/')
        self.assertEqual(response.status_code, 200)

    def test_if_url_education_is_exist(self):
        response = Client().get('/education/')
        self.assertEqual(response.status_code, 200)

    def test_if_url_skills_is_exist(self):
        response = Client().get('/skills/')
        self.assertEqual(response.status_code, 200)

    def test_if_url_register_is_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_if_url_schedule_is_exist(self):
        response = Client().get('/schedule/')
        self.assertEqual(response.status_code, 200)

    def test_if_url_schedule_add_is_exist(self):
        response = Client().get('/schedule/add/')
        self.assertEqual(response.status_code, 200)

    def test_if_using_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)

    def test_if_using_register_function(self):
        found = resolve('/register/')
        self.assertEqual(found.func, views.register)

    def test_if_using_skills_function(self):
        found = resolve('/skills/')
        self.assertEqual(found.func, views.skills)

    def test_if_using_contact_function(self):
        found = resolve('/contactme/')
        self.assertEqual(found.func, views.contact)

    def test_if_using_education_function(self):
        found = resolve('/education/')
        self.assertEqual(found.func, views.education)

    def test_if_using_schedule_function(self):
        found = resolve('/schedule/')
        self.assertEqual(found.func, views.schedule)

    def test_if_using_add_schedule_function(self):
        found = resolve('/schedule/add/')
        self.assertEqual(found.func, views.add_schedule)

    def test_if_using_register_validation_function(self):
        found = resolve('/register/validation/')
        self.assertEqual(found.func, views.validate_register)

    def test_form_validation_for_blank_items(self):
            form = forms.UserForm(data={'name' : '', 'email' : '', 'password' : ''})
            self.assertFalse(form.is_valid())
            self.assertEqual(
                form.errors['name'],
                ["This field is required."]
            )

    def test_success_if_url_is_json(self):
        response = self.client.post('/register/validation/')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'is_taken': False}
        )

    def test_if_post2_user_success(self):
        test_nama = 'Ujango'
        test_email = "ujango@gmail.com"
        test_password = "sayasukadjango"
        response_post = Client().post('/register/', {'name' : test_nama, 'email' : test_email, 'password' : test_password})
        self.assertEqual(response_post.status_code, 302)

    def test_if_post_schedule_failed(self):
        response_post = Client().post('/schedule/add/', 
        {'nama_kegiatan' : "Senam", 'tempat' : 'Lapangan Gor', 'kategori' : "Olahraga", 
        "tanggal" : now, "jam" : now})
        self.assertEqual(response_post.status_code, 200)

    def test_models2_if_on_the_database(self):
        new_activity = models.UserModel.objects.create(name='Ujango', email='ujango@gmail.com', password=123123123)

        counting_all_available_todo = models.UserModel.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_models_if_on_the_database(self):
        new_activity = models.Jadwal.objects.create(nama_kegiatan='Senam',\
         tempat='Lapangan GOR', kategori="Olahraga", jam=now(), tanggal=now())

        counting_all_available_todo = models.Jadwal.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_if_using_index_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'story4_app/index.html')

    def test_if_url_is_not_exist(self):
        response = Client().get('/wek')
        self.assertEqual(response.status_code, 404)

    