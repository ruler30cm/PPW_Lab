"""story4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from story4_app import views
from django.conf import settings
from django.conf.urls.static import static

app_name = "story4_app"

urlpatterns = [
    path('', views.index, name='index'),
    path('experiences/', views.experiences, name='experiences'),
    path('education/', views.education, name='education'),
    path('skills/', views.skills, name='skills'),
    path('contactme/', views.contact, name='contact'),
    path('register/', views.register, name='register'),
    path('subscriber/', views.subscriber, name='subscriber'),
    path('schedule/', views.schedule, name='schedule'),
    path('schedule/add/', views.add_schedule, name="add_schedule"),
    path('schedule/delete/', views.delete_schedule, name="delete_schedule"),
    path('register/validation/', views.validate_register, name="validate_register"),
    path('admin/', admin.site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
